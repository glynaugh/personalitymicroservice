
using Amazon.Lambda.Core;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace PersonalityMicroservice
{

    public class Body
    {
        public string inputstring { get; set; }
    }

    public class RootObject
    {
        public Body body { get; set; }
    }

    public class Function
    {
       
        public string FunctionHandler(RootObject input, ILambdaContext context)
        {
            
                var inputstring = input.body.inputstring;
                var databaseInfo = new DatabaseInfo();
                var textAnalyzer = new TextAnalyzer(databaseInfo.wordPersonalityCorrelations, databaseInfo.wordFrequencies, databaseInfo.numberOfWordsInEachPersonalityCategory);
                var topCategoriesAndWords = textAnalyzer.getTopPersonalityCategoriesFromUserInput(inputstring);
                var json = JsonConvert.SerializeObject(topCategoriesAndWords);
                return json;
        }
    }
}
