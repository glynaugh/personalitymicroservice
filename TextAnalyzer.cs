﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;


namespace PersonalityMicroservice
{

    public class TextAnalyzer
    {
        /*Get data from the DatabaseInfo class, if desired.*/
        public Dictionary<string, Dictionary<string, string>> wordPersonalityCorrelations { get; set; }
        public Dictionary <string, int> wordFrequencies { get; set; }
        public Dictionary<string, int> numberOfWordsInEachPersonalityCategory { get; set; }

        public TextAnalyzer(Dictionary<string, Dictionary<string, string>> wordCorrelations, Dictionary<string, int> wordFrequenciesDictionary, Dictionary<string, int> wordCategoryCounts)
        {
            wordPersonalityCorrelations = wordCorrelations;
            wordFrequencies = wordFrequenciesDictionary;
            numberOfWordsInEachPersonalityCategory = wordCategoryCounts;
        }


        public Dictionary<string, int> countSpecificWords(string comment)
        {
            var wordCountDictionary = new Dictionary<string, int>();
            foreach (string word in wordPersonalityCorrelations.Keys)
            {
                Regex wordRegex = new Regex("(?i)(?<![\\w-'])" + word + "\\b");
                var matches = wordRegex.Matches(comment);
                int numberOfMatches = wordRegex.Matches(comment).Count;
                if (numberOfMatches > 0)
                {
                    wordCountDictionary[word] = numberOfMatches;
                }
            }
            return wordCountDictionary;
        }

        public Dictionary<string, double> countCategoriesWithWeightedWordCounts(Dictionary<string, int> wordCounts)
        {
            /*Counts number of (weighted) words in each personality category. Returns string, double due to each word being assigned a "weight" based on
             1) the number of words in each personality category and 2) the frequency of the word. */ 
            var wordCategoriesCount = new Dictionary<string, double>();
            foreach (var wordAndCount in wordCounts)
            {
                var categoriesOfWord = wordPersonalityCorrelations[wordAndCount.Key];
                int singleWordCount = wordAndCount.Value;
                foreach (string category in categoriesOfWord.Keys)
                {
                    int numberOfWordsInCategory = numberOfWordsInEachPersonalityCategory[category]; //
                    int wordFrequency = wordFrequencies[wordAndCount.Key];
                    //The number of matches for the word is divided by the number of words in each category and multiplied by the word frequency.
                    double wordTimesWeight = Convert.ToDouble(singleWordCount) / Convert.ToDouble(numberOfWordsInCategory) * wordFrequency;
                    if (wordCategoriesCount.ContainsKey(category))
                    {
                        wordCategoriesCount[category] += wordTimesWeight;
                    }
                    else
                    {
                        wordCategoriesCount.Add(category, wordTimesWeight);
                    }
                }
            }
            return wordCategoriesCount;
        }

        public static List<string> getTopCategories(Dictionary<string, double> categoriesFromWordCounts)
        {
            List<KeyValuePair<string, double>> categoriesAndCounts = categoriesFromWordCounts.ToList();
            categoriesAndCounts.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));
            var numberOfCategories = categoriesAndCounts.Count;
            int maxNumberOfCategories;
            if (numberOfCategories >= 10)
            {
                maxNumberOfCategories = 10;
            }
            else
            {
                maxNumberOfCategories = numberOfCategories;
            }
            List<KeyValuePair<string, double>> topTenCategoriesAndCounts = categoriesAndCounts.GetRange(0, maxNumberOfCategories);
            List<string> topTenCategories = new List<string>();
            foreach (var categoryAndCount in topTenCategoriesAndCounts)
            {
                topTenCategories.Add(categoryAndCount.Key);
            }
            return topTenCategories;
        }

        public static List<KeyValuePair<string, int>> sortWordCounts(Dictionary<string, int> wordCounts)
        {
            List<KeyValuePair<string, int>> wordAndCounts = wordCounts.ToList();
            wordAndCounts.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value));
            return wordAndCounts;
        }

        public Dictionary<string, List<string>> getTopWordsInTopCategories(Dictionary<string, int> wordCounts, List<string> topCategories)
        {
            var categoriesAndTopWords = new Dictionary<string, List<string>>();
            var copyOfTopCategories = new List<string>(topCategories);
            var sortedWordCounts = sortWordCounts(wordCounts);
            foreach (var category in copyOfTopCategories)
            {
                categoriesAndTopWords.Add(category, new List<string>());
            }
            foreach (var wordAndCount in sortedWordCounts)
            {
                var categoriesToRemove = new List<string>();
                foreach (var category in copyOfTopCategories)
                {
                    if (wordPersonalityCorrelations[wordAndCount.Key].ContainsKey(category))
                    {
                        categoriesAndTopWords[category].Add(wordAndCount.Key);
                        if (categoriesAndTopWords[category].Count == 3)
                        {
                            categoriesToRemove.Add(category);
                        }
                    }
                }
                foreach (var category in categoriesToRemove)
                {
                    copyOfTopCategories.Remove(category);
                }
            }
            return categoriesAndTopWords;

        }

        public Dictionary<string, List<string>> getTopPersonalityCategoriesFromUserInput(string inputstring)
        {
            var wordCountDictionary = countSpecificWords(inputstring);
            var categoryCounts = countCategoriesWithWeightedWordCounts(wordCountDictionary);
            var topTenCategories = TextAnalyzer.getTopCategories(categoryCounts);
            var topCategoriesAndWords = getTopWordsInTopCategories(wordCountDictionary, topTenCategories);
            return topCategoriesAndWords;
        }

    }
}
