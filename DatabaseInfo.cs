﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;

namespace PersonalityMicroservice
{
    public class DBRootObject
    {
        public DbConnection dbConnection;
    }

    public class DbConnection
    {
        public string connectionString;
    }

    public class DatabaseInfo
    {
        public Dictionary<string, Dictionary<string, string>> wordPersonalityCorrelations { get; set; }
        public Dictionary<string, int> wordFrequencies { get; set; }
        public Dictionary<string, int> numberOfWordsInEachPersonalityCategory { get; set; }

        static Dictionary<string, Dictionary<string, string>> generateWordCorrelationsDictionary(SqlDataReader reader)
        {
            var wordDictionary = new Dictionary<string, Dictionary<string, string>>();
            while (reader.Read())
            {
                var dictOfCategories = new Dictionary<string, string>();
                string word = reader["Word"].ToString();
                string wordCategoryPrefix = "wordCategory";
                string wordCategoryName;
                string wordCategoryFromDatabase;
                for (int i = 1; i <= 7; i++)
                {
                    wordCategoryName = wordCategoryPrefix + i.ToString();
                    wordCategoryFromDatabase = reader[wordCategoryName].ToString();
                    if (wordCategoryFromDatabase != "")
                    {
                        dictOfCategories.Add(wordCategoryFromDatabase, null);
                    }
                }

                wordDictionary.Add(word, dictOfCategories);

            }
            return wordDictionary;
        }

        public Dictionary<string, int> generateNumberOfWordsInEachPersonalityCategoryDictionary(SqlDataReader reader)
        {
            var numberOfWordsInEachCategory = new Dictionary<string, int>();
            while (reader.Read())
            {
                string category = reader["wordcategory"].ToString();
                int numberOfWords = Int32.Parse(reader["numberofwords"].ToString());
                numberOfWordsInEachCategory.Add(category, numberOfWords);
            }
            return numberOfWordsInEachCategory;
        }

        public Dictionary<string, int> generateStringIntDictionary(SqlDataReader reader, string columnStr, string columnInt)
        {
            var stringIntDictionary = new Dictionary<string, int>();
            while (reader.Read())
            {
                string columnStrValue = reader[columnStr].ToString();
                int columnIntValue = Int32.Parse(reader[columnInt].ToString());
                stringIntDictionary.Add(columnStrValue, columnIntValue);
            }
            return stringIntDictionary;
        }

        public DatabaseInfo()
        {
            try
            {

                var objWithConnectionString = JsonConvert.DeserializeObject<DBRootObject>(File.ReadAllText("launchSettings.json"));
                var connectionStringFromFile = objWithConnectionString.dbConnection.connectionString;

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = connectionStringFromFile;

                string queryString = "select * from dbo.wordCorrelations";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = queryString;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        wordPersonalityCorrelations = generateWordCorrelationsDictionary(reader);
                    }
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        wordFrequencies = generateStringIntDictionary(reader, "word", "wordfrequency");
                    }
                    command.CommandText = "select * from dbo.wordCategories";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        numberOfWordsInEachPersonalityCategory = generateStringIntDictionary(reader, "wordcategory", "numberofwords");
                    }

                }

            }
            catch (SqlException e)
            {
                throw e;
            }
        }

    }
}
